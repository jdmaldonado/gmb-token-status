'use strict';

const express = require('express');
const processService = require('./services/process.service');

let app = express();
app.set('port', 8080);

app.listen(app.get('port'), () => {
  console.log('Express server listening on port... ' + app.get('port'));

  // processService.setInvalidTokens();
  // processService.generateTokenAndClientsJson();

  // processService.refreshTokens();
  // processService.updateLegacyClients();
  
 
});