'use strict'

const log4js = require('log4js');

log4js.configure({
  appenders: {
    file: {
      type: 'file',
      filename: 'jsons.log',
    },
    out: {
      type: 'stdout'
    }
  },
  categories: {
    default: {
      appenders: ['file', 'out'],
      level: 'trace'
    }
  }
});


const logger = log4js.getLogger();

const logJsonError = function logJsonError(error, id) {
  logger.error(`${id} ${error}`);
}

module.exports = {
  logJsonError
}