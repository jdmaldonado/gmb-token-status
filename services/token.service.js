'use strict';

const asyncLoop = require('node-async-loop');
const request = require('request');
const logService = require('./log.service');

const _getGetOptions = function _getGetOptions(client) {
  return {
    headers: {
      'Content-Type': 'application/json',
      'Cache-Control': 'no-cache',
      'x-api-token': client.xApiToken
    },
    method: 'GET',
    url: `http://p.lssdev.com/gmbtokens/${client.gmbTokenId}`,
    json: true
  }
}

const _getPostOptions = function _getPostOptions(client) {
  return {
    headers: {
      'Content-Type': 'application/json',
      'Cache-Control': 'no-cache',
      'x-api-token': client.xApiToken
    },
    method: 'POST',
    url: `http://p.lssdev.com/system/lambda/gmb`,
    json: true,
    body: {
      collection: 'locationList',
      client_id: client.clientId,
      action: 'get',
      env: 'prod',
      completeList: true
    }
  }
}

const _getPutOptions = function _getPutOptions(client, data) {
  let body = data || {
    status: 'invalid'
  };
  if (typeof client === 'string') {
    client = {
      xApiToken: `0ac1536b82dd638a4c3369b439b7df11`,
      gmbTokenId: client
    }
  }
  return {
    headers: {
      'Content-Type': 'application/json',
      'Cache-Control': 'no-cache',
      'x-api-token': client.xApiToken
    },
    method: 'PUT',
    url: `http://p.lssdev.com/gmbtokens/${client.gmbTokenId}`,
    json: true,
    body: body
  }
}

const _getRemoveOptions = function _getRemoveOptions(gmb_token) {
  return {
    headers: {
      'Content-Type': 'application/json',
      'Cache-Control': 'no-cache',
      'x-api-token': `0ac1536b82dd638a4c3369b439b7df11`
    },
    method: 'DELETE',
    url: `http://p.lssdev.com/gmbtokens/${gmb_token}`,
    json: true,
  }
}

const _hasInvalidToken = function _hasInvalidToken(client) {
  return new Promise((resolve, reject) => {
    getClientTokenStatus(client)
      .then((body) => {
        let isInvalid = _isInvalidToken(body);
        resolve(isInvalid);
      })
      .catch((err) => {
        reject();
      })
  });
}

const _isInvalidToken = function _isInvalidToken(body) {
  if (body && body.data && body.data.errorMessage && body.data.errorMessage === '"invalid_grant"') {
    return true;
  } else {
    return false;
  }
}

const _removeToken = function _removeToken(gmb_token) {
  return new Promise((resolve, reject) => {
    let options = _getRemoveOptions(gmb_token);

    request(options, (err, response, body) => {
      if (body && body.error) {
        reject(`Error Removing Token -- gmb_token:'${gmb_token}' ... ${body.error}`);
      } else {
        resolve(body)
      }
    });
  });
}

const _updateClient = function _updateClient(client, data) {
  return new Promise((resolve, reject) =>  {

    let options = _getPutOptions(client, data);


    request(options, (err, response, body) => {
      if (body && body.error) {
        reject(`Error Updating Client ${client} - ${client.gmbTokenId} ... ${body.error}`);
      } else {
        resolve(body)
      }
    });

  })
}

const getClientTokenStatus = function getClientTokenStatus(client) {
  return new Promise((resolve, reject) =>  {

    let options = _getPostOptions(client);

    request(options, (err, response, body) => {
      if (err) {
        reject(err);
      } else {
        resolve(body)
      }
    });

  })
}

const removeUpdateTokens = function removeUpdateTokens(tokens) {
  return new Promise((resolve, reject) => {
    let count = 1;
    asyncLoop(tokens, (token, next) => {
      console.log(`trying... ${count} of ${tokens.length} -- ${token.gmb_token}`);
      if (token.delete) {
        _removeToken(token.gmb_token)
          .then(() => {
            count++;
            console.log('-------------');
            next();
          })
          .catch((err) => {
            count++;
            logService.logJsonError(err, token.gmb_token)
            next();
          })
      } else {
        let data = {
          account: token.account
        }
        _updateClient(token.gmb_token, data)
          .then(() => {
            count++;
            console.log('-------------');
            next();
          })
          .catch((err) => {
            count++;
            logService.logJsonError(err, token.gmb_token)
            next();
          })
      }
    }, (err) => {
      if (err) {
        reject(err);
      } else {
        resolve();
      }
    });
  });
}

const refreshClientsToken = function refreshClientsToken(clients) {
  return new Promise((resolve, reject) => {
    let count = 1;
    asyncLoop(clients,
      (clientKeys, next) => {
        let client = {
          clientId: clientKeys[0],
          gmbTokenId: clientKeys[1],
          xApiToken: clientKeys[2]
        }
        console.log(`trying... ${count} of ${clients.length} -- ${client.clientId} -- ${client.gmbTokenId}`);
        
        _hasInvalidToken(client)
          .then((isInvalid) => {
            if (isInvalid) {
              logService.logJsonError(`${client.clientId} - ${client.gmbTokenId} - INVALID`, client.clientId);
              return _updateClient(client)
            } else {
              logService.logJsonError(`${client.clientId} - ${client.gmbTokenId} - VALID`, client.clientId);
              return _updateClient(client, {
                status: `valid`
              })
            }
          })
          .then((result) => {
            count++;
            next();
          })
          .catch((err) => {
            logService.logJsonError(`ERROR:` + err, client.clientId)
            count++;
            next();
          })


      }, (err) => {
        if (err) {
          reject(err)
        } else {
          resolve()
        }
      });

  })
}

module.exports = {
  refreshClientsToken,
  removeUpdateTokens
}