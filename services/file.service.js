'use strict';

const fs = require('fs');
const parse = require('csv-parse');
const path = require('path');

const filesPath = path.resolve(__dirname, '..', `files`);

const _writeFile = function _writeFile(path, fileName, data) {
  return new Promise((resolve, reject) => {
    fs.writeFile(`${path}/${fileName}`, data || 'Not Data Found', (err) => {
      if (err) {
        reject(err);
      } else {
        resolve();
      }
    });
  });
};

const createJson = function createJson(data, fileName) {

  const config = JSON.stringify(data, null, 2);

  return new Promise((resolve, reject) => {
    fileName = fileName || 'newFile.json';

    _writeFile(filesPath, fileName, config)
      .then(_ => resolve())
      .catch(err => reject(err))

  });
}

const getCSVData = function getCSVData(fileName) {
  return new Promise((resolve, reject) => {
    if (!fileName) {
      reject({
        message: 'CSV File name is required'
      });
    } else {
      let csvData = [];
      const filePath = `${filesPath}/${fileName}`;

      fs.createReadStream(filePath)
        .pipe(parse({
          delimiter: ','
        }))
        .on('data', (csvrow) => {
          if (csvrow[0] !== 'client_id') {
            csvData.push(csvrow);
          }
        })
        .on('end', () => {
          console.log('Finish CSV reading');
          resolve(csvData)
        });
    }

  });
}

const getJsonData = function getJsonData(filename) {
  const pathConfig = require(`${filesPath}/${filename}`);
  return pathConfig;
}

module.exports = {
  createJson,
  getCSVData,
  getJsonData,
}