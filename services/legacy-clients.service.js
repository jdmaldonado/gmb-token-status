'use strict';

const _ = require('lodash');
const asyncLoop = require('node-async-loop');
const request = require('request');
const logService = require('./log.service');

const _getInfoFromUrl = function _getInfoFromUrl(url) {
  const invalidInfo = {
    account: -1,
    location: null
  }
  if (url && typeof url === 'string') {
    let splited = url.split('/');
    if (splited.length >= 4) {
      return {
        account: splited[1],
        location: `locations/${splited[3]}`
      }
    } else {
      return invalidInfo;
    }
  } else {
    return invalidInfo;
  }
}

const _getLegacyClientsRequestOptions = function _getLegacyClientsRequestOptions(gmb_token) {
  return {
    headers: {
      'Content-Type': 'application/json',
      'x-api-token': '0ac1536b82dd638a4c3369b439b7df11'
    },
    method: 'GET',
    url: `http://p.lssdev.com/legacyclients?where={"gmb_token":"${gmb_token}"}`,
    json: true
  }
}

const _getLegacyClientUpdateOptions = function _getLegacyClientUpdateOptions(legacyClient, data) {
  return {
    headers: {
      'Content-Type': 'application/json',
      'x-api-token': legacyClient.xApiToken
    },
    method: 'PUT',
    url: `http://p.lssdev.com/legacyclients/${legacyClient.clientId}`,
    json: true,
    body: data
  }
}

const _getPartnerRequestOptions = function _getPartnerRequestOptions(userName) {
  return {
    headers: {
      'Content-Type': 'application/json',
      'x-api-token': '0ac1536b82dd638a4c3369b439b7df11'
    },
    method: 'GET',
    url: `http://p.lssdev.com/legacypartners?username=${userName}`,
    json: true
  }
}

const _getLegacyClientByGmbToken = function _getLegacyClientByGmbToken(gmb_token) {
  return new Promise((resolve, reject) => {
    if (!gmb_token) {
      reject({
        message: 'GMB Token is required'
      })
    } else {
      let options = _getLegacyClientsRequestOptions(gmb_token);

      request(options, (err, response, body) => {
        if (err) {
          reject(err);
        } else {
          resolve(body)
        }
      });
    }
  })
}

const _getLegacyClientInfoByGMBToken = function _getLegacyClientInfoByGMBToken(gmb_token) {
  return new Promise((resolve, reject) => {
    _getLegacyClientByGmbToken(gmb_token)
      .then((client) =>  {
        if (client && client.data && client.data.length > 0) {

          let clientId = client.data[0].id || -1;
          let partnerUserName = client.data[0].partnerUsername || null;

          resolve({
            clientId,
            partnerUserName
          });
        } else {
          resolve(-1);
        }
      })
      .catch((err) => {
        reject(err)
      })
  })
}

const _getPartnerByUserName = function _getPartnerByUserName(userName) {
  return new Promise((resolve, reject) => {
    if (!userName) {
      reject({
        message: 'userName is required'
      })
    } else {
      let options = _getPartnerRequestOptions(userName);

      request(options, (err, response, body) => {
        if (err) {
          reject(err);
        } else {
          resolve(body);
        }
      });
    }
  })
}

const _getPartnerXApiToken = function _getPartnerXApiToken(body) {
  if (body && body.data && body.data.length > 0) {
    return body.data[0].apiKey || null;
  } else {
    return null;
  }
}

const _orderTokensByDate = function _orderTokensByDate(tokensArray, order)  {
  return _.orderBy(tokensArray, 'updatedAt', order || 'desc');
}

const _getNewJsonPropeties = function _getNewJsonPropeties(tokensArray) {
  return _.map(tokensArray, (item) => {
    let info = _getInfoFromUrl(item.location)
    return {
      account: info.account,
      delete: item.delete,
      email: item.email,
      gmb_location: item.location,
      gmb_token: item.id,
      updatedAt: item.updatedAt
    }

  })
}

const _setDeleteTokens = function _setDeleteTokens(tokensArray) {
  for (let i = 0; i < tokensArray.length; i++) {
    tokensArray[i].delete = (i !== 0);
  }
  return tokensArray;
}

const _updateLegacyClient = function _updateLegacyClient(client, data) {
  return new Promise((resolve, reject) => {

    if (client.clientId === -1) {
      reject(`The clientId is invalid clientId:'${client.clientId}' -- gmb_token:'${client.gmb_token}'`);
    } else {
      if (!client.gmb_location) {
        delete data.gmb_location;
      }

      let options = _getLegacyClientUpdateOptions(client, data)

      request(options, (err, response, body) => {
        if (err) {
          reject(`Error Updating Client clientId:'${client.clientId}' -- gmb_token:'${client.gmb_token}' ... ${err}`);
        } else {
          resolve(body)
        }
      });
    }


  });
}

const evaluateClientsIdFromTokens = function evaluateClientsIdFromTokens(tokenList, tokensWithStatus) {
  return new Promise((resolve, reject) => {
    let newLegacyClients = [];
    let count = 1;

    asyncLoop(tokenList,
      (tokenData, next) => {
        console.log(`trying... ${count} of ${tokenList.length} -- ${tokenData.id}`);

        let legacyClientData;

        _getLegacyClientInfoByGMBToken(tokenData.id)
          .then((clientData) => {
            legacyClientData = clientData;
            if (clientData && clientData.partnerUserName) {
              return _getPartnerByUserName(clientData.partnerUserName);
            } else {
              return Promise.reject(`There is not Info For Legacy Client with GMB Token "${tokenData.id}" `)
            }
          })
          .then((partnerData) => {
            let info = _getInfoFromUrl(tokenData.location);
            newLegacyClients.push({
              clientId: legacyClientData.clientId,
              gmb_token: tokensWithStatus.grouped[tokenData.email][0].gmb_token,
              gmb_token_old: tokenData.id,
              gmb_location: info.location,
              email: tokenData.email,
              xApiToken: _getPartnerXApiToken(partnerData)
            });
            count++
            next();
          })
          .catch((err) => {
            logService.logJsonError(err, tokenData.id);
            count++;
            next();
          })


      }, (err) => {
        if (err) {
          reject(err)
        } else {
          resolve(newLegacyClients)
        }
      });

  })
}

const evaluateTokensToRemove = function evaluateTokensToRemove(tokenList) {
  return new Promise((resolve, reject) => {
    try {
      let groupedTokens = _.groupBy(tokenList, 'email');

      groupedTokens = _.mapValues(groupedTokens, (item) => {
        return _getNewJsonPropeties(_setDeleteTokens(_orderTokensByDate(item)));
      })

      let tokensArray = _.flattenDeep(_.toArray(groupedTokens));

      let tokens = {
        grouped: groupedTokens,
        array: tokensArray
      }

      resolve(tokens);

    } catch (error) {
      reject(error);
    }


  });

}

const updateLegacyClientsLoop = function updateLegacyClientsLoop(legacyClients) {
  return new Promise((resolve, reject) => {
    let count = 0;
    asyncLoop(legacyClients, (client, next) => {
      console.log(`trying... ${count} of ${legacyClients.length} -- ${client.clientId}`);
      console.log(client);
      let data = {
        gmb_token: client.gmb_token,
        gmb_location: client.gmb_location
      }
      _updateLegacyClient(client, data)
        .then(() => {
          count++;
          next();
        })
        .catch((err) => {
          count++;
          logService.logJsonError(err, client.gmb_token);
          next();
        });
    }, (err) => {
      if (err) {
        reject();
      } else {
        resolve();
      }
    });
  });
}

module.exports = {
  evaluateClientsIdFromTokens,
  evaluateTokensToRemove,
  updateLegacyClientsLoop,
}