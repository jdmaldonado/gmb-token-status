'use strict';

const fileService = require('./file.service');
const legacyClientService = require('./legacy-clients.service');
const tokenService = require('./token.service');

// Generate the new Jsons Of Tokens And LegacyClientes
const generateTokenAndClientsJson = function generateTokenAndClientsJson() {

  let gmbTokens = fileService.getJsonData('GMB_tokens_backup_28_02_15_53.1.json');
  let tokenWithStatus;

  legacyClientService.evaluateTokensToRemove(gmbTokens)
    .then((tokensData) => {
      tokenWithStatus = tokensData;
      return fileService.createJson(tokensData.array, 'generated/new_tokens_status.json');
    })
    .then(() => {
      console.log('Tokens Status new json created !!!!');
      return legacyClientService.evaluateClientsIdFromTokens(gmbTokens, tokenWithStatus)
    })
    .then((legacyClientsData) => {
      return fileService.createJson(legacyClientsData, 'generated/new_legacy_clients_with_valid_token.json');
    })
    .then(() => {
      console.log('Legacy client new json created !!!!');
      console.log('Finishhhhh');
    })
    .catch((err) => console.log('err', err));
}

// Remove the Invalid tokens and Add account to valid ones
const refreshTokens = function deleteInvalidTokens() {
  let gmbTokens = fileService.getJsonData('generated/new_tokens_status.json');

  tokenService.removeUpdateTokens(gmbTokens)
    .then((result) => {
      console.log('Finish!!!');
      console.log('result', result);
    })
    .catch((err) => {
      console.log('err', err);
    })
}

// Update Legacy Clients from new Json Info
const updateLegacyClients = function updateLegacyClients() {
  // let legacyClientsData = fileService.getJsonData('generated/new_legacy_clients_with_valid_token.json');
  let legacyClientsData = fileService.getJsonData('generated/without_gmb_location.json');

  legacyClientService.updateLegacyClientsLoop(legacyClientsData)
    .then((result) => {
      console.log('Finish!!!');
      console.log('result', result);
    })
    .catch((err) => {
      console.log('err', err);
    })
}

// set invalid status in Token Data
const setInvalidTokens = function setInvalidTokens() {
  fileService.getCSVData('gmb_tokens_to_verify.1.csv')
    .then((data) => {
      return tokenService.refreshClientsToken(data);
    })
    .then((result) => {
      console.log('result', result);
    })
    .catch((err) => {
      console.log('err', err);
    })
}

module.exports = {
  generateTokenAndClientsJson,
  refreshTokens,
  setInvalidTokens,
  updateLegacyClients
}